﻿
using Microsoft.AspNetCore.SignalR;

namespace SignalR.signalR
{
    public class RealtimeHub : Hub
    {
        public static Dictionary<string, string> activeConnections = new Dictionary<string, string>();

        /// <summary>
        /// Send Message To User From From End Jqeury Function
        /// </summary>
        /// <param name="user"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task SendMessage(string user, string message)
        {
            //this method return message to receiver 
            await Clients.Others.SendAsync("ReceiveMessage", user, message);
        }

        /// <summary>
        /// Set User To Dictionary
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public async Task ActiveConnection(string username)
        {
            var signalRId = Context.ConnectionId;
            activeConnections.Add(username, signalRId);
            await Clients.Caller.SendAsync("ActivateConnection", true);
        }

        public override Task OnConnectedAsync()
        {
            return base.OnConnectedAsync();
        }
        public override Task OnDisconnectedAsync(Exception? exception)
        {
            return base.OnDisconnectedAsync(exception);
        }
    }
}
